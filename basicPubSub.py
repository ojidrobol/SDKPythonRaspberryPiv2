'''
/*
 * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 '''

from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import logging
import time
import argparse
import json
import random
import math
from datetime import date
from datetime import datetime

AllowedActions = ['both', 'publish', 'subscribe']

# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message: ")
    print(message.payload)
    print("from topic: ")
    print(message.topic)
    print(f'El mensaje fue:{message}')
    print("--------------\n\n")

## Permite Obtener Numero aleatorio
def numeroAleatorio():
    [ale,ale_ent]=math.modf(random.random()*pow(10,10))
    return ale_ent

## Permite Obtener Numero aleatorio
def obtenerMedicion(Valorminimo,Valormaximo):
    return random.randrange(Valorminimo,Valormaximo)

## Obtiene parametros
def obtenerParametros():
    with open('configuration.json') as file:
        configurations = json.load(file)
        for configuration in configurations['configuration']:
            sensorId=configuration['sensorId']
            host=configuration['host']
            topic=configuration['topic']
            name_device=configuration['name_device']
            measurement=configuration['measurement']
            rootCAPath=configuration['Certificado']
            certificatePath=configuration['LlaveCertificado']
            privateKeyPath=configuration['Llaveprivada']
            name_device=configuration['name_device']
            Valorminimo=configuration['Valorminimo']
            Valormaximo=configuration['Valormaximo']
            timeread=configuration['timeread']
            return sensorId,host,topic,name_device,measurement,rootCAPath,certificatePath,privateKeyPath,name_device,Valorminimo,Valormaximo,timeread
        


# Read in command-line parameters
parser = argparse.ArgumentParser()
#parser.add_argument("-e", "--endpoint", action="store", required=True, dest="host", help="Your AWS IoT custom endpoint")
#parser.add_argument("-r", "--rootCA", action="store", required=True, dest="rootCAPath", help="Root CA file path")
parser.add_argument("-c", "--cert", action="store", dest="certificatePath", help="Certificate file path")
parser.add_argument("-k", "--key", action="store", dest="privateKeyPath", help="Private key file path")
parser.add_argument("-p", "--port", action="store", dest="port", type=int, help="Port number override")
parser.add_argument("-w", "--websocket", action="store_true", dest="useWebsocket", default=False,
                    help="Use MQTT over WebSocket")
parser.add_argument("-id", "--clientId", action="store", dest="clientId", default="basicPubSub",
                    help="Targeted client id")
#parser.add_argument("-t", "--topic", action="store", dest="topic", default="sdk/test/Python", help="Targeted topic")
parser.add_argument("-t", "--topic", action="store", dest="topic", default="gateway1/response/read/humedad1", help="Targeted topic")
parser.add_argument("-m", "--mode", action="store", dest="mode", default="both",
                    help="Operation modes: %s"%str(AllowedActions))
parser.add_argument("-M", "--message", action="store", dest="message", default="Hello World!",help="Message to publish")

args = parser.parse_args()
#host = args.host
#rootCAPath = args.rootCAPath
#certificatePath = args.certificatePath
#privateKeyPath = args.privateKeyPath
port = args.port
useWebsocket = args.useWebsocket
clientId = args.clientId
topic = args.topic
##name_device=None
# Carga parametros de dispositivos
sensorId,host,topic,name_device,measurement,rootCAPath,certificatePath,privateKeyPath,name_device,Valorminimo,Valormaximo,timeread=obtenerParametros()

if args.mode not in AllowedActions:
    parser.error("Unknown --mode option %s. Must be one of %s" % (args.mode, str(AllowedActions)))
    exit(2)

if args.useWebsocket and certificatePath and privateKeyPath:
    parser.error("X.509 cert authentication and WebSocket are mutual exclusive. Please pick one.")
    exit(2)

if not args.useWebsocket and (not certificatePath or not privateKeyPath):
    parser.error("Missing credentials for authentication.")
    exit(2)

# Port defaults
if args.useWebsocket and not args.port:  # When no port override for WebSocket, default to 443
    port = 443
if not args.useWebsocket and not args.port:  # When no port override for non-WebSocket, default to 8883
    port = 8883

# Configure logging
logger = logging.getLogger("AWSIoTPythonSDK.core")
logger.setLevel(logging.DEBUG)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)


# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
if useWebsocket:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId, useWebsocket=True)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath)
else:
    myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
    myAWSIoTMQTTClient.configureEndpoint(host, port)
    myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
if args.mode == 'both' or args.mode == 'subscribe':
    respuestasubcripcion=myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
    print(f'La subscripcion al topico {topic} fue : {respuestasubcripcion}')
time.sleep(2)

# Publish to the same topic in a loop forever
loopCount = 0
while True:
    Fecha=datetime.now()
    if args.mode == 'both' or args.mode == 'publish':
        message = {}
        ## message['message'] = args.message
        ##message['sequence'] = loopCount

        ## Contruye Mensaje para publicar en Topico de Humedad 
        message['sensorId'] = sensorId
        message['datetime'] = f"{Fecha.year}-{Fecha.month}-{Fecha.day} {Fecha.hour}:{Fecha.minute}:{Fecha.second}"
        message['Mode'] = 'read'
        message['Idmessage'] = numeroAleatorio()
        message['sensor_read__response'] =sensor_read__response={}
        sensor_read__response['sensorName']=name_device
        sensor_read__response['topic']=topic
        sensor_read__response['Value']=obtenerMedicion(Valorminimo,Valormaximo)
        sensor_read__response['measurement']=measurement
        
       ## Contruye Mensaje para publicar en Topico de Humedad 
        messageJson = json.dumps(message)
        print(f"El valor del topico es: {args.message} ")
        print(f"El valor del topico es: {topic} ")
        print(f"El valor del mode es:{args.mode}")
        print(f"El mensaje publicado sera:{messageJson}")
        obtenerParametros()
        ##2020-11-23 20:04:50
        respuestapublicacion=myAWSIoTMQTTClient.publish(topic, messageJson, 1)
        #print(f'la publicacion del mensaje fue:{respuestapublicacion}')
        if args.mode == 'publish':
            print('Published topic %s: %s\n' % (topic, messageJson))
        loopCount += 1
    time.sleep(timeread)

